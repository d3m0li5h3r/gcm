package com.d3m0li5h3r.libs.gcm;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.d3m0li5h3r.libs.gcm.services.GcmRegisterService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.lang.ref.WeakReference;

/**
 * Created by Sandeep S. Saini on 24/6/15.
 */
public class GcmClient {
    public static final int CODE_REQUEST = 0xFFFFE;

    private WeakReference<? extends Activity> mActivityReference;

    /**
     * Requires the calling activity to register/unregister BroadcastReceivers
     * to receive REGISTER/UNREGISTER events
     *
     * @param activity
     */

    public GcmClient(Activity activity) {
        mActivityReference = new WeakReference<>(activity);
    }

    /**
     * method to check whether the device has Google Play Services available or not.
     *
     * @param showDialog boolean param to show the error dialog or not.
     * @return returns boolean whether Google Play Services is available or not.
     */
    public boolean isPlayServicesAvailable(boolean showDialog) {
        return isPlayServicesAvailable(showDialog, null);
    }

    /**
     * method to check whether the device has Google Play Services available or not.
     *
     * @param showDialog       boolean param to show the error dialog or not.
     * @param onCancelListener listener to get the dialog cancelled callback.
     * @return returns boolean whether Google Play Services is available or not.
     */
    public boolean isPlayServicesAvailable(boolean showDialog,
                                           DialogInterface.OnCancelListener onCancelListener) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        Activity activity = mActivityReference.get();
        if (null != activity) {
            int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);

            switch (status) {
                case ConnectionResult.SUCCESS:
                    return true;
                default:
                    if (showDialog) {
                        if (null == onCancelListener)
                            googleApiAvailability.getErrorDialog(activity, status, CODE_REQUEST)
                                    .show();
                        else
                            googleApiAvailability.getErrorDialog(activity, status, CODE_REQUEST,
                                    onCancelListener).show();
                    }

                    return false;
            }
        } else
            return false;
    }

    public void register(String projectId) {
        if (TextUtils.isEmpty(projectId))
            throw new NullPointerException("projectId cannot be null/blank.");

        Context context = mActivityReference.get();

        if (null != context)
            GcmRegisterService.startActionGcmRegister(context, projectId);
    }

    public void register(String projectId, boolean checkForPlayServices) {
        if (checkForPlayServices && isPlayServicesAvailable(true))
            register(projectId);
    }

    public void unregister(String projectId) {
        if (TextUtils.isEmpty(projectId))
            throw new NullPointerException("projectId cannot be null/blank.");

        Context context = mActivityReference.get();

        if (null != context)
            GcmRegisterService.startActionGcmUnregister(context, projectId);
    }

    public void unregister(String projectId, boolean checkForPlayServices) {
        if (checkForPlayServices && isPlayServicesAvailable(true))
            unregister(projectId);
    }

    public String getToken() {
        Context context = mActivityReference.get();

        if (null != context) {
            SharedPreferences preferences = context.getSharedPreferences(
                    GcmRegisterService.NAME_PREFS_GCM, Context.MODE_PRIVATE);

            return preferences.getString(GcmRegisterService.KEY_TOKEN, "");
        }

        return "";
    }
}
