package com.d3m0li5h3r.libs.gcm.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.lang.ref.WeakReference;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class GcmRegisterService extends IntentService {
    public static final String NAME_PREFS_GCM = "gcm-preferences";
    public static final String KEY_TOKEN = "gcm_token";
    public static final String ACTION_GCM_REGISTER = "com.d3m0li5h3r.libs.gcm.services.action.REGISTER";
    public static final String ACTION_GCM_UNREGISTER = "com.d3m0li5h3r.libs.gcm.services.action.UNREGISTER";
    private static final String EXTRA_GCM_ID_PROJECT = "com.d3m0li5h3r.libs.gcm.extras.ID_PROJECT";

    private static WeakReference<Context> mContextReference;

    public GcmRegisterService() {
        super("GcmRegisterService");
    }

    /**
     * Starts this service to perform action Register with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionGcmRegister(Context context, String projectId) {
        mContextReference = new WeakReference<>(context);

        Intent intent = new Intent(context, GcmRegisterService.class);
        intent.setAction(ACTION_GCM_REGISTER);
        intent.putExtra(EXTRA_GCM_ID_PROJECT, projectId);

        context.startService(intent);
    }

    /**
     * Starts this service to perform action Unregister with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionGcmUnregister(Context context, String projectId) {
        mContextReference = new WeakReference<>(context);

        Intent intent = new Intent(context, GcmRegisterService.class);
        intent.setAction(ACTION_GCM_UNREGISTER);
        intent.putExtra(EXTRA_GCM_ID_PROJECT, projectId);

        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (null != intent) {
            final String action = intent.getAction();
            switch (action) {
                case ACTION_GCM_REGISTER:
                    handleActionGcmRegister(intent.getStringExtra(EXTRA_GCM_ID_PROJECT));
                    break;
                case ACTION_GCM_UNREGISTER:
                    handleActionGcmUnregister(intent.getStringExtra(EXTRA_GCM_ID_PROJECT));
                    break;
            }
        }
    }

    /**
     * Handle action Register in the provided background thread with the provided
     * parameters.
     */
    private void handleActionGcmRegister(String projectId) {
        Context context = mContextReference.get();
        if (null != context) {
            try {
                InstanceID instanceID = InstanceID.getInstance(context);
                String token = instanceID.getToken(projectId,
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE);

                SharedPreferences preferences = context.getSharedPreferences(NAME_PREFS_GCM,
                        Context.MODE_PRIVATE);
                preferences.edit().putString(KEY_TOKEN, token).apply();

                Intent intent = new Intent(ACTION_GCM_REGISTER);
                intent.putExtra(KEY_TOKEN, token);
                context.sendBroadcast(intent);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            } catch (Exception e) {
                // Do nothing
            }
        }
    }

    /**
     * Handle action Unregister in the provided background thread with the provided
     * parameters.
     */
    private void handleActionGcmUnregister(String projectId) {
        Context context = mContextReference.get();
        if (null != context) {
            try {
                InstanceID instanceID = InstanceID.getInstance(context);
                instanceID.deleteToken(projectId, GoogleCloudMessaging.INSTANCE_ID_SCOPE);

                SharedPreferences preferences = context.getSharedPreferences(NAME_PREFS_GCM,
                        Context.MODE_PRIVATE);
                preferences.edit().remove(KEY_TOKEN).apply();

                Intent intent = new Intent(ACTION_GCM_UNREGISTER);
                context.sendBroadcast(intent);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            } catch (Exception e) {
                // Do nothing
            }
        }
    }
}
